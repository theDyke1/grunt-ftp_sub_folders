/*
 * grunt-ftp_sub_folders
 * https://github.com/jamesdyke/grunt-ftp_sub_folders
 *
 * Copyright (c) 2016 James Dyke
 * Licensed under the MIT license.
 */

'use strict';

var fs = require('fs'),
  JSFtp = require("jsftp"),
  JSFtp = require('jsftp-mkdirp')(JSFtp),
  chalk = require('chalk'),
  walk = require('walk');

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask('ftp_sub_folders', 'uploads to FTP with subfolders', function() {
   
    // Merge task-specific and/or target-specific options with these defaults.
    var srcFolder = this.files[0].orig.cwd;  //cwd
    var path = this.files[0].orig.dest+'/';  //dest
    var destiationPath = '';
    var done = this.async();

    var Ftp = new JSFtp({
      host: this.options().host,
      port: this.options().port,
      user: this.options().user,
      pass: this.options().pass
    });

    var init = function() {
      putFilesUp();
    }

    var mkFolderPath = function(folder) {
      var re = /\/\/(.+)/;
      

      if ((re.exec(folder)) !== null) {
        var res = {};
        res.folder = re.exec(folder)[1];
        res.destiationPath = path + res.folder;

      }
      return res;
    };

    var putFilesUp = function() {
      var walker = walk.walk(srcFolder, {
        followLinks: false
      });

      walker.on('file', function(root, stat, next) {
       
        var folderPath = mkFolderPath(root);
        if (/\.png|\.jpg|\.gif/.test(stat.name)) {

          Ftp.mkdirp(folderPath.destiationPath, function(err) {
            if (err) {
              throw err;

            } else {
              var ftpDestination = srcFolder + folderPath.folder + stat.name;
                Ftp.put(srcFolder + folderPath.folder + '/' + stat.name, folderPath.destiationPath + '/' + stat.name, function(hadError) {
                  if (!hadError) {
                    console.log(chalk.green('✔ ') + 'Uploaded file to: ' + chalk.magenta(folderPath.destiationPath + '/' + stat.name));
                    next();
                  } else {
                    grunt.warn(hadError + ' in file ');
                    next();
                  }
                });
            }
          });
        } else {
          next();
        }
      });
      walker.on('end', function() {
        console.log('FTP upload complete')
        process.exit();
      })
    }
    init();
  });
};
