/*
 * grunt-ftp_sub_folders
 * https://github.com/jamesdyke/grunt-ftp_sub_folders
 *
 * Copyright (c) 2016 James Dyke
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Configuration to be run (and then tested).
    ftp_sub_folders: {
       options: {
            "host": "",
            "port": ,
            "user": "",
            "pass": ""
        },
        files: [{
            expand: true,
            cwd: 'dist/img/',
            src: ['**/*.{png,jpg,jpeg,gif}'],
            dest:'<%= grunt.template.today("yyyy/mm") %>'

        }]
    }
  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // By default, lint and run all tests.
  grunt.registerTask('default', ['ftp_sub_folders']);

};
