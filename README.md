# grunt-ftp_sub_folders

> FTP all subfolders from your specified src to FTP.  This patch creates the all the subfolders from the src to FTP.

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-ftp_sub_folders --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-ftp_sub_folders');
```

## The "ftp_sub_folders" task

### Overview
In your project's Gruntfile, add a section named `ftp_sub_folders` to the data object passed into `grunt.initConfig()`.

```js
// Project configuration.
  grunt.initConfig({
    // Configuration to be run (and then tested).
    ftp_sub_folders: {
       options: {
            "host": "",
            "port": ,    
            "user": "",  //user id
            "pass": ""   //password
        },
        files: [{
            expand: true,
            cwd: 'src/',
            src: ['**/*.{png,jpg,jpeg,gif}'],  //you can define file types to FTP here
            dest:'dest/'  //your FTP destination folder  

        }]
    }
  });
```

## Release History
04/30/2106 - Slightly better readme update..
